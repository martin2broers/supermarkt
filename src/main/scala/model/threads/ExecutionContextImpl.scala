package model.threads

import java.util.concurrent.{Executors, ThreadPoolExecutor}

import org.apache.logging.log4j.scala.Logging

import scala.concurrent.ExecutionContext

class ExecutionContextImpl extends ExecutionContext with Logging{
  val executor = Executors.newCachedThreadPool()

  override def execute(runnable: Runnable): Unit = executor.submit(runnable)

  override def reportFailure(cause: Throwable): Unit = logger.error("Error in thread: ", cause)
}
