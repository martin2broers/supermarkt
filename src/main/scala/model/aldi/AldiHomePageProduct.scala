package model.aldi

import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element

class AldiHomePageProduct(el: Element) extends model.vendors.Product {

  title = el >> text("h3 a")

  url = el >> element("h3 a") >> attr("href")

  description = ""

  tags = List()

  private val p = "([0-9]+.)".r
  private val content = el >> text(".price-tag .main a strong")
  private val first = p.findFirstIn(content)

  price = if (first.isDefined) first.get.toDouble else -1.0
  vendor = "Aldi"

}
