package model.aldi

import java.io.File
import java.net.URL
import javax.inject.Singleton

import com.google.inject.Inject
import model.vendors.{Vendor, VendorManager}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._

import scala.collection.mutable.ArrayBuffer
import scala.sys.process._
import scala.util.control.Breaks._

@Singleton
class AldiParser @Inject()(vendorManager: VendorManager) extends Aldi with Vendor {
  val browser = JsoupBrowser()
  vendorManager.registerVendor(this) // first register self with manager

  val aldiDir = new File("aldi")
  if (!aldiDir.exists())
    aldiDir.mkdir()


  override def getProducts: Seq[model.vendors.Product] = {
    //offers offer
    val r = ArrayBuffer.empty[model.vendors.Product]

    val doc = downloadIfNotExists("https://www.aldi.nl/")

    val items = doc >> elementList(".offers .offer")

    val navItems = doc >> elementList("#navigation > ul > li")

    val secondNavItem = navItems(1)
    val secondNavItems = secondNavItem >> elementList("ul li")
    breakable {
      for (item <- secondNavItems) {
        var a = item >> element("a")
        var url = a >> attr("href")
        val inner = downloadIfNotExists("https://www.aldi.nl/" + url)
        val productlist = inner >> elementList(".productlist > div")
        for (product <- productlist) {
          val p: model.vendors.Product = new AldiProduct(product)
          r += p
        }
      }
    }
    /*
    for (item <- items) {
      r += new AldiHomePageProduct(item)
    }*/
    r
  }

  override def name: String = "Aldi"


  private def downloadIfNotExists(URL: String) = {
    val url = new URL(URL)
    var mainSite: File = null
    if (url.getPath == "" || url.getPath == "/") {
      mainSite = new File("aldi/index.html")
    } else {
      mainSite = new File("aldi/" + url.getPath)
    }
    if (!mainSite.exists())
      url #> mainSite !!

    if (url.getPath == "" || url.getPath == "/") {
      browser.parseFile("aldi/index.html")
    } else {
      browser.parseFile("aldi/" + url.getPath)
    }
  }
}
