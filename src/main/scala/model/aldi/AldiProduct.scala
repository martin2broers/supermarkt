package model.aldi

import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import org.apache.logging.log4j.scala.Logger

class AldiProduct(private val el: Element) extends model.vendors.Product {
  private val logger = Logger(getClass)

  private val t = el >> text("h3 a")

  private var pric = 0.0

  try{
    val priceString = el >> text(".price-tag strong")
    if(priceString != ""){
      pric = priceString.toDouble
    }
  }catch {
    case e: Throwable => logger.warn("Could not parse price", e)
  }

  vendor = "Aldi"
  title = t
  price = pric

  val _id = el >> element(".product-social-links")
  id = _id >> attr("id")

}
