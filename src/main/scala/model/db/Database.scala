package model.db

import scala.concurrent.Future

trait Database {
  def getByID(id: String): Future[Option[model.vendors.Product]]

  def getByName(name: String): Future[Seq[model.vendors.Product]]

  def getByTags(tags: Seq[String]): Future[Seq[model.vendors.Product]]

  def setProducts(products: Seq[model.vendors.Product]): Future[Unit]
}
