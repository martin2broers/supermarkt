package model.db

import java.io.{File, PrintWriter}
import javax.inject.{Inject, Singleton}

import model.vendors
import model.vendors.JsonProducts
import org.apache.logging.log4j.scala.Logging
import play.api.libs.json.Json

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class DatabaseImpl @Inject()()(implicit executionContext: ExecutionContext) extends Database with JsonProducts with Logging {
  private val _cache = mutable.TreeMap.empty[String, model.vendors.Product]
  private val _tags = mutable.TreeMap.empty[String, mutable.HashSet[String]]


  val f = new File("db.json")

  val importJson = Future {
    if (f.exists()) {
      val lines = scala.io.Source.fromFile("db.json").mkString
      val maybeProducts = Json.fromJson[Seq[model.vendors.Product]](Json.parse(lines))

      if (maybeProducts.isSuccess) {
        for (item <- maybeProducts.get) {
          _processProduct(item)
        }
      } else {
        logger.error("parsing json DB failed", null)
      }
    } else {
      f.createNewFile()
    }
  }

  /**
    * Store products in
    *
    * @param product
    */
  private def _processProduct(product: vendors.Product) = {
    for(tag <- product.tags){
      if (_tags.contains(tag)) {
        _tags(tag) += product.id
      } else {
        val hashSet = mutable.HashSet(tag)
        _tags += tag -> hashSet
      }
      1 //workaround for bug #SI-10151
    }
    _cache += product.id -> product
  }

  override def getByID(id: String): Future[Option[vendors.Product]] = if (importJson.isCompleted) Future.successful(_cache.get(id)) else importJson.map(r => _cache.get(id))

  override def getByName(name: String): Future[Seq[vendors.Product]] = if (importJson.isCompleted) Future.successful(_cache.filter(_._2.title == name).values.toSeq) else importJson.map(r => _cache.filter(_._2.title == name).values.toSeq)

  override def getByTags(tags: Seq[String]): Future[Seq[vendors.Product]] = ???

  override def setProducts(products: Seq[vendors.Product]): Future[Unit] = Future {
    products.foreach(_processProduct)
    synchronized {
      //saving is sensitive
      val json = Json.toJson(_cache.values)
      val pw = new PrintWriter(f)
      pw.write(json.toString())
      pw.close()
    }
  }
}
