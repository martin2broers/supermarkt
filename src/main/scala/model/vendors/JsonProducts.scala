package model.vendors

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json._

trait JsonProducts {
  val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val jodaDateReads = Reads[DateTime](js =>
    js.validate[String].map[DateTime](dtString =>
      DateTime.parse(dtString, DateTimeFormat.forPattern(dateFormat))
    )
  )
  implicit val jodaDateWrites: Writes[DateTime] = (d: DateTime) => JsString(d.toString())

  implicit lazy val JsonDiscountReads: Reads[Discount] = (
    (JsPath \ "price").read[Double] and
      (JsPath \ "from").read[DateTime] and
      (JsPath \ "until").read[DateTime]
    ) (Discount.apply _)

  implicit lazy val JsonDiscountWrites: Writes[Discount] = (
    (JsPath \ "price").write[Double] and
      (JsPath \ "from").write[DateTime] and
      (JsPath \ "until").write[DateTime]
    ) (unlift(Discount.unapply))


  implicit lazy val JsonProductReads: Reads[model.vendors.Product] = (
    (JsPath \ "id").read[String] and
    (JsPath \ "vendor").read[String] and
      (JsPath \ "title").read[String] and
      (JsPath \ "url").read[String] and
      (JsPath \ "description").read[String] and
      (JsPath \ "from").read[DateTime] and
      (JsPath \ "until").read[DateTime] and
      (JsPath \ "tags").read[Seq[String]] and
      (JsPath \ "price").read[Double] and
      (JsPath \ "discount").readNullable[Discount]
    ) (model.vendors.Product.apply _)


  implicit lazy val JsonProductWrites: Writes[model.vendors.Product] = (
    (JsPath \ "id").write[String] and
    (JsPath \ "vendor").write[String] and
      (JsPath \ "title").write[String] and
      (JsPath \ "url").write[String] and
      (JsPath \ "description").write[String] and
      (JsPath \ "from").write[DateTime] and
      (JsPath \ "until").write[DateTime] and
      (JsPath \ "tags").write[Seq[String]] and
      (JsPath \ "price").write[Double] and
      (JsPath \ "discount").writeNullable[Discount]
    ) (unlift(model.vendors.Product.unapply))

}
