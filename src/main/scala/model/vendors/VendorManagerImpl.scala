package model.vendors

import javax.inject.{Inject, Singleton}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

@Singleton
class VendorManagerImpl @Inject()() extends VendorManager {
  private val vendors = mutable.TreeMap.empty[String, Vendor]

  override def getVendors: Seq[Vendor] = vendors.values.toList

  override def getVendor(name: String): Option[Vendor] = vendors.get(name)

  override def registerVendor(v: Vendor): Unit = vendors += v.name -> v
}
