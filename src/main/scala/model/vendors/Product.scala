package model.vendors

import org.joda.time.DateTime
import play.api.libs.json.Json

case class Product(
                             var id: String = "",
                             var vendor: String = "",
                             var title: String  = "",
                             var url: String = "",
                             var description: String = "",
                             var from: DateTime = DateTime.now(),
                             var until: DateTime = DateTime.now(),
                             var tags: Seq[String] = List(),
                             var price: Double = 0.0,
                             var discount: Option[Discount] = None
                  ) extends JsonProducts {

  def toJson: String = Json.toJson(this).toString()
}

case class Discount(price: Double, from: DateTime, until: DateTime)