package model.vendors

trait VendorManager {
  def registerVendor(v: Vendor)
  def getVendors: Seq[Vendor]
  def getVendor(name: String): Option[Vendor]
}
