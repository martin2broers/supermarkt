package model.vendors

trait Vendor {
  def name: String
  def getProducts: Seq[Product]
}
