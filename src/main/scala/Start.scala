import java.io.IOException

import com.google.inject.Guice
import module.MainModule

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafxml.core.FXMLView


object FXMLAdoptionForm extends JFXApp {
  import scalafx.Includes._
  implicit  val injector = Guice.createInjector(new MainModule)

  val resource = getClass.getResource("view/primary.fxml")
  if (resource == null) {
    throw new IOException("Cannot load resource: primary.fxml")
  }

  val root = FXMLView(resource, new GuiceDependencyResolver())

  stage = new PrimaryStage() {
    title = "SuperMarkt"
    width = 800
    height = 450
    scene = new Scene(root)
  }

}
