package controller

import com.google.inject.Inject
import model.db.Database
import model.vendors.VendorManager
import org.apache.logging.log4j.scala.Logging

import scalafx.event.ActionEvent
import scalafxml.core.macros.sfxml

@sfxml
class Primary(vendorManager: VendorManager, db: Database) extends Logging{

  // event handlers are simple public methods:
  def onCreate(event: ActionEvent) {
    // ...
    logger.info("Application startup")
    logger.info("Settings main Guice")

    val vendors = vendorManager.getVendors

    logger.info("listing vendors")
    for(vendor <- vendors){
      logger.info(s"processing vendor: ${vendor.name}")
      val products = vendor.getProducts
      db.setProducts(products)
      for(product <- products){
        logger.info(s"product ${product.title} price: ${product.price}")
        logger.info(s"product ${product.title} description: ${product.description}")
        logger.info(s"product ${product.title} url: ${product.url}")
      }
    }
  }
}
