package module

import com.google.inject.AbstractModule
import model.aldi.{Aldi, AldiParser}
import model.db.{Database, DatabaseImpl}
import model.threads.ExecutionContextImpl
import model.vendors.{VendorManager, VendorManagerImpl}

import scala.concurrent.ExecutionContext

class MainModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[VendorManager]).to(classOf[VendorManagerImpl])
    bind(classOf[Aldi]).to(classOf[AldiParser])
    bind(classOf[Database]).to(classOf[DatabaseImpl])
    bind(classOf[ExecutionContext]).to(classOf[ExecutionContextImpl])
  }
}
